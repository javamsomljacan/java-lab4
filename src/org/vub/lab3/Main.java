package org.vub.lab3;


public class Main {

    public static void main(String[] args) {
	var lokacija = new Location("Pliva", "Coner", "Mali Coner", "Gunduliceva 15");
    var medicine1 = new Medicine("Voltaren", 5);
    var medicine2 = new Medicine("Brufen", 10);
    var medicine3 = new Medicine("Lupocet", 11);
    var material1 = new Material("Voda", 10);
    var material2 = new Material("Prasak", 2);
    var material3 = new Material("Aloa vera", 8);
    var rukavice = new ZastitneRukavice("crne",50);
    var maske = new ZastitneMaske(50);

    lokacija.ispis();
    medicine1.ispis();
    medicine2.ispis();
    medicine3.ispis();
    material1.ispis();
    material2.ispis();
    material3.ispis();
    rukavice.ispis();
    maske.ispis();

    }
}
