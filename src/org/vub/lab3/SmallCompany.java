package org.vub.lab3;

public class SmallCompany extends BigCompany{
    public String imeKompanije;

    public SmallCompany(String imeVelikeKompanije, String imeKompanije){
        super(imeVelikeKompanije);
        this.imeKompanije=imeKompanije;
    }
}
