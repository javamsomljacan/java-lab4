package org.vub.lab3;

public class Material {
    public String material;
    public int broj;

    public Material(String materijal, int broj){
        this.material=materijal;
        this.broj=broj;
    }

    public void ispis(){
        System.out.println(material + ": " + broj);
    }
}
