package org.vub.lab3;

public class Location extends DrugStore {
    public String lokacija;

    public Location(String imeVelikeKompanije, String imeKompanije, String imePoslovnice, String lokacija){
        super(imeVelikeKompanije, imeKompanije, imePoslovnice);
        this.lokacija=lokacija;
    }

    public void ispis(){
        System.out.println(imeVelikeKompanije + "->" + imeKompanije + "->" + imePoslovnice + "->" + lokacija);
    }
}
